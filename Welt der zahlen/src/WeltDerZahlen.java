/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstraße
     long  anzahlSterne = 100000000000l;
    
     
    // Wie viele Einwohner hat Berlin?
      int  bewohnerBerlin = 3769000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
      short alterTage = 6205;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    float  gewichtKilogramm = 1900000f;  
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
     long  flaecheGroessteLand = 17100000l;
    
    // Wie groß ist das kleinste Land der Erde?
     float  flaecheKleinsteLand = 0.44f;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne : " + anzahlSterne );
    
    System.out.println("Anzahl der Einwohner von berlin : " + bewohnerBerlin );
    
    System.out.println("Anzahl meines Alters in Tagen : " + alterTage );
    
    System.out.println("Das Gewicht des schwersten Tieres der Welt : " + gewichtKilogramm );
    
    System.out.println("Gr��e das Gr��tem Landes der Welt  : " + flaecheGroessteLand );
    
    System.out.println("Gr��e des kleinstem Landes : " + flaecheKleinsteLand );
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

